// permasalahan 
const nama ='jimin';
const nama2 = 'jungkook';
const nama3 = 'jaehyun';

// array
// jimin , jungkook , jaehyun
//   0   |   1      |    2    
const mahasiswa =['jimin','jungkook','jaehyun'];

// akses dengan index
console.log(mahasiswa);
console.log(mahasiswa[3]);

// memotong array
console.log(mahasiswa.slice(0, 2));
console.log(mahasiswa.slice(1, 2));

// mengubah isi array
mahasiswa[3] = 'witri';
console.log(mahasiswa);

// mengecek array
console.log(mahasiswa.includes('Witri'));

